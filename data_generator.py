import matplotlib
import matplotlib.pyplot as plt
import matplotlib.figure as fig
import matplotlib.patches as mpatches
from IPython.display import clear_output
from matplotlib.pyplot import imshow
import scipy.misc
import time
import os
import gc
import copy
import numpy as np
from PIL import Image
from scipy.io import loadmat
import random
import sys
import keras.backend as K
import tensorflow as tf
from keras.utils.np_utils import to_categorical

def load_image_nyu(idx, file_path=None):
    mean_bgr = np.array((116.190, 97.203, 92.318), dtype=np.float32)
    img = Image.open(file_path.format(idx))
    in_ = np.array(img, dtype=np.float32)
    #img.show()
    in_ = in_[:,:,::-1]
    in_ -= mean_bgr    
    #if in_.any() == 1 :
    #    print(idx)
    img.close()
    return in_

def load_label_nyu(idx, file_path=None,flip_flag=False):   
    label_dict = label_processing()
    
    if flip_flag==False:
        label = loadmat(file_path.format(idx))['groundTruth'][0][0]['SegmentationClass'][0][0].astype(np.uint8)
    elif flip_flag==True:
        label = loadmat(file_path.format(idx))['seg'].astype(np.uint8)
        
    new_tmp=[]
    for row in label:
        new_row=[]
        for lbl in row:
            new_row.append(label_dict.get(lbl))
        new_tmp.append(new_row)
    gc.collect()
    label_new = np.stack(new_tmp, axis=0).astype(np.uint8)
    label_new -= 1
    #label_cat = to_categorical(label_new)
    label_ = label_new[..., np.newaxis]
    #imshow(label_new)
    return label_

def nyu_data_genarator(idx_dict, batch_size, img_file_path, lbl_file_path, flip_flag=False):
    images_ = np.zeros((batch_size,425,560,3))
    labels_ = np.zeros((batch_size,425,560,1))
    sample_pool = idx_dict.copy()
    while True: 
        if len(sample_pool) < batch_size :
            sample_pool = idx_dict.copy()
            print('\n Refilled')
            
        samples = random.sample(sample_pool.keys(), batch_size)

        for idx, item in enumerate(samples):
            images_[idx] = load_image_nyu(sample_pool.get(item), file_path=img_file_path)
            labels_[idx] = load_label_nyu(sample_pool.get(item), file_path=lbl_file_path,flip_flag=flip_flag)
            sample_pool.pop(item, None)
            
        X_batch = images_
        y_batch = labels_
        
        yield (X_batch, y_batch) 

def get_test_nyu(test_list=None, img_path=None, lbl_path=None):
    images_ = []
    labels_ = []
    for item in test_list:
        images_.append(load_image_nyu(item, img_path))
        labels_.append(load_label_nyu(item, lbl_path))    

    test_img = np.stack(images_, axis=0)
    test_lbl = np.stack(labels_, axis=0)

    return test_img, test_lbl
def get_batch_nyu(train_dict=None, val_dict=None, img_path=None, lbl_path=None):
    images_ = []
    labels_ = []
    images_val = []
    labels_val = []
    for item in train_dict:
        images_.append(load_image_nyu(item, img_path))
        labels_.append(load_label_nyu(item, lbl_path))
    for obj in val_dict:
        images_val.append(load_image_nyu(obj, img_path))
        labels_val.append(load_label_nyu(obj, lbl_path))
        
    train_img = np.stack(images_, axis=0)
    train_lbl = np.stack(labels_, axis=0)   
    val_img = np.stack(images_val, axis=0)
    val_lbl = np.stack(labels_val, axis=0)
    
    return train_img, train_lbl, (val_img, val_lbl)
    
def label_processing():
    classes = loadmat('data/benchmarkData/metadata/classMapping40.mat')['mapClass'][0]
    class_dict = {}
    for idx, val in enumerate(classes):
        class_dict[idx+1] = val
    class_dict[0] = 41
    return class_dict




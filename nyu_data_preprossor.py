import random
import keras
import numpy as np
from keras import backend as K
from keras.callbacks import ReduceLROnPlateau,ModelCheckpoint,CSVLogger
from scipy.io import loadmat
import gc
from PIL import Image

def load_image_nyu(idx):
    mean_bgr = np.array((116.190, 97.203, 92.318), dtype=np.float32)
    img = Image.open('data/colorImage/img_{}.png'.format(idx))
    in_ = np.array(img, dtype=np.float32)
    #img.show()
    in_ = in_[:,:,::-1]
    in_ -= mean_bgr
    #in_ = in_.transpose((2,0,1))
    #print(in_.shape)
    img.close()

    return in_

def load_label_nyu(idx):   
    label_dict = label_processing()
    label = loadmat('data/benchmarkData/groundTruth/img_{}.mat'.format(idx))['groundTruth'][0][0]['Segmentation'][0][0]
    #print(label.shape)
    #im = Image.fromarray(label)
    #im.show()  
    new_tmp=[]
    for row in label:
        new_row=[]
        for lbl in row:
            new_row.append(label_dict.get(lbl))
        new_tmp.append(new_row)
    gc.collect()
    label_new = np.stack(new_tmp, axis=0)
    label_new -= 1
    label_ = label_new[..., np.newaxis]
#   label_out = np.asarray(label_)
#    print(idx,np.amin(label_out),np.amax(label_out))
#    label_cat = to_categorical(label_, 40)
#    label_out = label_cat.reshape(425, 560, 40)
    
    return label_

def label_processing():
    classes = loadmat('data/benchmarkData/metadata/classMapping40.mat')['mapClass'][0]
    class_dict = {}
    for idx, val in enumerate(classes):
        class_dict[idx+1] = val
    class_dict[0] = 40
    return class_dict

def get_batch_nyu(train_pmt='train', val_pmt='val'):
    split = loadmat('data/benchmarkData/metadata/nyusplits.mat')
    train_idx = split[train_pmt][0]
    val_idx = split[val_pmt][0]
    gc.collect()
    images_ = []
    labels_ = []
    images_val = []
    labels_val = []
    for item in train_idx:
        images_.append(load_image_nyu(item))
        labels_.append(load_label_nyu(item))
    for obj in val_idx:
        images_val.append(load_image_nyu(obj))
        labels_val.append(load_label_nyu(obj))
        
    train_img = np.stack(images_, axis=0)
    train_lbl = np.stack(labels_, axis=0)   
    val_img = np.stack(images_val, axis=0)
    val_lbl = np.stack(labels_val, axis=0)
    
    return train_img, train_lbl, (val_img, val_lbl)


from keras.utils.data_utils import get_file
import copy
import numpy as np
from PIL import Image
from scipy.io import loadmat
from keras import backend as K
from keras.models import Model, Sequential, load_model
from keras.preprocessing.image import load_img, ImageDataGenerator, img_to_array, array_to_img, pil_image
from keras.optimizers import RMSprop, SGD, Adam
from keras.metrics import categorical_accuracy
from keras.callbacks import ReduceLROnPlateau,ModelCheckpoint,CSVLogger, LearningRateScheduler
from keras.preprocessing.image import ImageDataGenerator
import random
import keras
import math
import os
from sklearn.model_selection import train_test_split

from .fcn32s_vgg16 import *
from .data_generator import *
from .fcn_score import *


class PeriodicLogger(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.iteration = 0
    def on_batch_end(self, batch, logs={}):
        self.iteration += 1
    def on_epoch_end(self, epoch, logs={}):
        print'\n Iteration: ',self.iteration

def render_model(resume_flag=False, opt=None, log=None, mdl_flag=None):
	if mdl_flag == 'R':
		mdl = fcn_Resnet50(input_shape=(425,560,3))
		mdl.compile(optimizer=opt,loss=[softmax_sparse_crossentropy_ignoring_last_label],metrics=[sparse_accuracy_ignoring_last_label])
		if resume_flag == True:
			mdl.load_weights('logs/Resnet50/{}/model.h5'.format(log))
			print('Loading weights from path.......')
		elif resume_flag == False:
			print('Constructing new model..........')
	elif mdl_flag == 'V':
		mdl = fcn_vggbase(input_shape=(425,560,3))
		mdl.compile(optimizer=opt,loss=[softmax_sparse_crossentropy_ignoring_last_label],metrics=[sparse_accuracy_ignoring_last_label])
		if resume_flag == True:
			mdl.load_weights('logs/VGG16/{}/model.h5'.format(log))
			print('Loading weights from path.......')
		elif resume_flag == False:
			print('Constructing new model..........')
	elif mdl_flag == 'DM':
		mdl = dilated_FCN_addmodule(input_shape=(425,560,3))
		mdl.compile(optimizer=opt,loss=[softmax_sparse_crossentropy_ignoring_last_label],metrics=[sparse_accuracy_ignoring_last_label])
	elif mdl_flag == 'DF':
		mdl = dilated_FCN_frontended(input_shape=(425,560,3))
		mdl.compile(optimizer=opt,loss=[softmax_sparse_crossentropy_ignoring_last_label],metrics=[sparse_accuracy_ignoring_last_label])
	mdl.summary()
	return mdl

def render_img_path(flag=False):
	if flag == True:
	    image_file_path = 'data/colorImage_flipped/img_{}.png'
	    label_file_path = 'data/benchmarkData/groundTruth_flipped/img_{}.mat'
	elif flag == False:
	    image_file_path = 'data/colorImage/img_{}.png'
	    label_file_path = 'data/benchmarkData/groundTruth/img_{}.mat'
	return (image_file_path, label_file_path)

def render_saving_path(mdl_flag=None, resume_flag=None, log=None):
	if mdl_flag == 'R':
		dir_path = 'logs/Resnet50/{}'.format(log)
		if not os.path.exists(dir_path):
			os.makedirs(dir_path)
		mdl_path = 'logs/Resnet50/{}/model.h5'.format(log)
		record_path_tmp = 'logs/Resnet50/{}'
		if resume_flag == True:
			record_path = record_path_tmp.format(log)+'/record_resume.csv'
		elif resume_flag == False:
			record_path = record_path_tmp.format(log)+'/record.csv'
	elif mdl_flag == 'V':
		dir_path = 'logs/VGG16/{}'.format(log)
		if not os.path.exists(dir_path):
			os.makedirs(dir_path)
		mdl_path = 'logs/VGG16/{}/model.h5'.format(log)
		record_path_tmp = 'logs/VGG16/{}'
		if resume_flag == True:
			record_path = record_path_tmp.format(log)+'/record_resume.csv'
		elif resume_flag == False:
			record_path = record_path_tmp.format(log)+'/record.csv'
	elif mdl_flag == 'DM':
		dir_path = 'logs/DilatedModuleFCN/{}'.format(log)
		if not os.path.exists(dir_path):
			os.makedirs(dir_path)
		mdl_path = 'logs/DilatedModuleFCN/{}/model.h5'.format(log)
		record_path_tmp = 'logs/DilatedModuleFCN/{}'
		if resume_flag == True:
			record_path = record_path_tmp.format(log)+'/record_resume.csv'
		elif resume_flag == False:
			record_path = record_path_tmp.format(log)+'/record.csv'
	elif mdl_flat == 'DF':
		dir_path = 'logs/DilatedFrontFCN/{}'.format(log)
		if not os.path.exists(dir_path):
			os.makedirs(dir_path)
		mdl_path = 'logs/DilatedFrontFCN/{}/model.h5'.format(log)
		record_path_tmp = 'logs/DilatedFrontFCN/{}'
		if resume_flag == True:
			record_path = record_path_tmp.format(log)+'/record_resume.csv'
		elif resume_flag == False:
			record_path = record_path_tmp.format(log)+'/record.csv'
	return mdl_path, record_path

def step_decay(epoch):
	initial_lrate = 1e-2
	drop = 0.1
	epochs_drop = 23.0
	lrate = initial_lrate * math.pow(drop, math.floor((1+epoch)/epochs_drop))
	return lrate

if __name__ == '__main__':
	###################panel##################
	train_pmt = 'trainval'
	batch_size= 1
	val_ratio= 0.2
	mdl_flag = str(sys.argv[1])
	flip_flag = eval(sys.argv[2])
	resume_flag = eval(sys.argv[3])
	log = sys.argv[4]
	print(sys.argv)
	IterNum = 50000

	opt = RMSprop(lr=1e-4)
	#opt = SGD(lr=1e-4, decay=0.0, momentum=0.99, nesterov=True)
	#opt = Adam(lr=1e-4)

	###########################################
	split = loadmat('data/benchmarkData/metadata/nyusplits.mat')
	train_idx_dict, val_idx_dict = {}, {}
	trainval_idx_list = split[train_pmt][0]
	train_idx_list, val_idx_list = train_test_split(trainval_idx_list, test_size=val_ratio, random_state=5)
	train_idx_dict = {idx:val for idx,val in enumerate(train_idx_list)}
	val_idx_dict = {idx:val for idx,val in enumerate(val_idx_list)}
	gc.collect()

	training_steps = len(train_idx_dict)
	epochs = round(IterNum/training_steps)
	val_steps = len(val_idx_dict)

	img_file_path = render_img_path(flag=flip_flag)
	model_path, record_path = render_saving_path(mdl_flag=mdl_flag, resume_flag=resume_flag, log=log)
	fcn = render_model(resume_flag=resume_flag, opt=opt, log=log, mdl_flag=mdl_flag)

	iterator = PeriodicLogger()
	#scheduler = LearningRateScheduler(step_decay)
	reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3, verbose =1, min_lr=1e-8)
	csv = CSVLogger(record_path, separator=',', append=True)
	model_check = ModelCheckpoint(model_path, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=True)

	callbacks_list = [iterator, reduce_lr,csv, model_check]

	fcn.fit_generator(
	    generator=nyu_data_genarator(train_idx_dict,
	                                 batch_size=batch_size,
	                                 img_file_path=img_file_path[0],
	                                 lbl_file_path=img_file_path[1],
	                                 flip_flag=flip_flag
	                                ),
	    steps_per_epoch=training_steps,
	    epochs=epochs,
	    verbose=1,
	    callbacks=callbacks_list,
	    validation_data=nyu_data_genarator(val_idx_dict,
	                                       batch_size=batch_size,
	                                       img_file_path=img_file_path[0],
	                                       lbl_file_path=img_file_path[1],
	                                       flip_flag=flip_flag
	                                      ),
	    validation_steps=val_steps
	    )
import numpy as np
import keras.backend as K
import tensorflow as tf

def get_confusion(a, b, n):
    """Compute the confusion matrix given two vectors and number of classes."""
    k = (a >= 0) & (a < n)
    return np.bincount(n*a[k].astype(int) + b[k],minlength=n**2).reshape(n, n)


def compute_error_matrix(y_true, y_pred):
    """Compute Confusion matrix (a.k.a. error matrix).
    a       predicted
    c       0   1   2
    t  0 [[ 5,  3,  0],
    u  1  [ 2,  3,  1],
    a  2  [ 0,  2, 11]]
    l
    Note true positves are in diagonal
    confusion = get_confusion(K.argmax(y_true, axis=ax_chn).flatten(),
                              K.argmax(y_pred, axis=ax_chn).flatten(),
                              classes)
    """
    # Find channel axis given backend
    '''
    nb_classes = K.int_shape(y_pred)[-1]
    y_true = K.one_hot(tf.to_int32(K.flatten(y_true)), nb_classes+1)
    unpacked = tf.unstack(y_true, axis=-1)
    legal_labels = ~tf.cast(unpacked[-1], tf.bool)
    y_true = tf.stack(unpacked[:-1], axis=-1)
    '''
    nb_classes = K.int_shape(y_pred)[-1]
    y_true = K.one_hot(tf.to_int32(K.flatten(y_true)), nb_classes+1)
    unpacked = tf.unstack(y_true, axis=-1)
    y_true = tf.stack(unpacked[:-1], axis=-1)
    y_true_confu_in = K.reshape(K.argmax(y_true, axis=-1), [-1])
    y_pred_confu_in = K.reshape(K.argmax(y_pred, axis=-1), [-1])
    confusion = get_confusion(y_true_confu_in, y_pred_confu_in, nb_classes)
    return confusion


def accuracy(y_true, y_pred):
    """Compute accuracy."""
    confusion = compute_error_matrix(y_true, y_pred)
    # per-class accuracy
    acc = np.diag(confusion).sum() / float(confusion.sum())
    return acc


def mean_accuracy(y_true, y_pred):
    """Compute mean accuracy."""
    confusion = compute_error_matrix(y_true, y_pred)
    # per-class accuracy
    acc = np.diag(confusion) / confusion.sum(1)
    return np.nanmean(acc)


def mean_IU(y_true, y_pred):
    """Compute mean IoU."""
    confusion = compute_error_matrix(y_true, y_pred)
    # per-class IU
    iu = np.diag(confusion) / ( confusion.sum(1) + confusion.sum(0)- np.diag(confusion) )

    return np.nanmean(iu)


def freq_weighted_IU(y_true, y_pred):
    """Compute frequent weighted IoU."""
    confusion = compute_error_matrix(y_true, y_pred)
    freq = confusion.sum(1) / float(confusion.sum())
    # per-class IU
    iu = np.diag(confusion) / (confusion.sum(1) + confusion.sum(0)- np.diag(confusion))

    return (freq[freq > 0] * iu[freq > 0]).sum()

def sparse_accuracy_ignoring_last_label(y_true, y_pred):
    nb_classes = K.int_shape(y_pred)[-1]
    y_pred = K.reshape(y_pred, (-1, nb_classes))
    y_true = K.one_hot(tf.to_int32(K.flatten(y_true)), nb_classes+1)
    unpacked = tf.unstack(y_true, axis=-1)
    legal_labels = ~tf.cast(unpacked[-1], tf.bool)
    y_true = tf.stack(unpacked[:-1], axis=-1)

    return K.sum(tf.to_float(legal_labels & K.equal(K.argmax(y_true, axis=-1), K.argmax(y_pred, axis=-1)))) / K.sum(tf.to_float(legal_labels))

#def mean_IU_ignoring_last_label(y_true, y_pred):


def softmax_sparse_crossentropy_ignoring_last_label(y_true, y_pred):
    nb_classes = K.int_shape(y_pred)[-1]
    y_pred = K.reshape(y_pred, (-1, nb_classes))
    log_softmax = tf.nn.log_softmax(y_pred)

    y_true = K.one_hot(tf.to_int32(K.flatten(y_true)), nb_classes+1)
    unpacked = tf.unstack(y_true, axis=-1)
    y_true = tf.stack(unpacked[:-1], axis=-1)

    cross_entropy = -K.sum(y_true * log_softmax, axis=-1)
    cross_entropy_mean = K.mean(cross_entropy)

    return cross_entropy_mean


def corssentropy2d(y_true, y_pred):
    num_class = K.int_shape(y_pred)[-1]
    _y_true = K.one_hot(tf.to_int32(K.flatten(y_true)), num_class)
    _y_pred = K.reshape(y_pred, (-1, num_class))
    log_softmax = tf.nn.log_softmax(_y_pred)

    return K.mean(-K.sum(_y_true * log_softmax, axis=-1))


def sparse_pixelwise_accuracy(y_true, y_pred):
    num_class = K.int_shape(y_pred)[-1]
    _y_pred = K.reshape(y_pred, (-1, num_class))
    _y_true = K.flatten(y_true)

    return K.mean(K.equal(_y_true, tf.to_float(K.argmax(_y_pred, axis=-1))))
